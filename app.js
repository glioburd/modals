let modal = null
const focusableSelector = 'button, a, input, textarea'
let focusableElements = []
let previouslyFocusedElement = null

const openModal = async function (e) {
	e.preventDefault()
	const target = e.target.getAttribute('href')
	if (target.startsWith('#')) {
		modal = document.querySelector(target)
	} else {
		modal = await loadModal(target)
	}
	focusableElements = Array.from(modal.querySelectorAll(focusableSelector))
	previouslyFocusedElement = document.querySelector(':focus')
	modal.style.display = null
	focusableElements[0].focus()
	modal.removeAttribute('aria-hidden')
	modal.setAttribute('aria-modal', 'true')
	modal.addEventListener('click', closeModal)
	modal.querySelector('.js-modal-close').addEventListener('click', closeModal)
	modal.querySelector('.js-modal-stop').addEventListener('click', stopPropagation)
}

const closeModal = function (e) {
	if (modal === null) return
	if (previouslyFocusedElement !== null ) previouslyFocusedElement.focus()
	e.preventDefault()

	modal.setAttribute('aria-hidden', 'true')
	modal.removeAttribute('aria-modal')
	modal.removeEventListener('click', closeModal)
	modal.querySelector('.js-modal-close').removeEventListener('click', closeModal)
	modal.querySelector('.js-modal-stop').removeEventListener('click', stopPropagation)

	/* Il remet direct un display 'none', donc pas d'animation css. Du coup : on attend la fin de l'animation */
	const hideModal = function () {
		modal.style.display = 'none'
		modal.removeEventListener('animationend', hideModal)
		modal = null
	}
	modal.addEventListener('animationend', hideModal)
}

const stopPropagation = function (e) {
	e.stopPropagation()
}

const focusInModal = function (e) {
	e.preventDefault()
	let index = focusableElements.findIndex(f => f === modal.querySelector(':focus'))
	if (e.shiftKey === true) {
		index--
	} else {
		index++
	}
	if (index >= focusableElements.length) {
		index = 0
	}
	if (index < 0) {
		index = focusableElements.length - 1
	}
	focusableElements[index].focus()
}

const loadModal = async function (url) {
	// TODO : Afficher un loader
	const target = '#' + url.split('#')[1]
	const existingModal = document.querySelector(target)
	if (existingModal !== null) return existingModal
	const html = await fetch(url).then(response => response.text())
	const element = document.createRange().createContextualFragment(html).querySelector(target)
	if (element === null ) throw `L'élément ${target} n'a pas été trouvé dans la page ${url}.`
	document.body.append(element)
	return element
}

document.querySelectorAll('.js-modal').forEach(a => {
	a.addEventListener('click', openModal)
})

window.addEventListener('keydown', function (e) {
	if (e.key === "Escape" || e.key === "Esc") {
		closeModal(e)
	}
	if (e.key === "Tab" && modal !== null) {
		focusInModal(e)
	}
})
